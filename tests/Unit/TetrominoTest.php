<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class TetrominoTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    public function testMoveDown() {
        // Array viejos (antes de mover)
        $data = [];
        $respuesta = $this->json('GET', "/api/get", $data);
        $respuesta->assertStatus(200);
        $respuesta->assertJsonStructure(['data'], $respuesta->json());
        $respuesta->assertJsonStructure(['blocks_coords', 'local_matrix'], $respuesta->json()['data']);
        $expected = array_map(function($item) {
            return [$item[0] - 1, $item[1]];
        }, $respuesta->json()['data']['blocks_coords']);

        // Arrays nuevos (después de mover)
        $response = $this->json('GET', "/api/move/down", $data);
        $response->assertStatus(200);
        $response->assertJsonStructure(['data'], $response->json());
        $response->assertJsonStructure(['blocks_coords', 'local_matrix'], $response->json()['data']);
        $actual = $response->json()['data']['blocks_coords'];

        $this->assertEquals($expected, $actual);
    }
    public function testMoveRight() {
        // Array viejos (antes de mover)
        $data = [];
        $respuesta= $this->json('GET', "/api/get", $data);
        $respuesta->assertStatus(200);
        $respuesta->assertJsonStructure(['data'], $respuesta->json());
        $respuesta->assertJsonStructure(['blocks_coords', 'local_matrix'], $respuesta->json()['data']);

        $expected = array_map(function($item) {
            return [$item[0], $item[1] + 1];
        }, $respuesta->json()['data']['blocks_coords']);

        // Arrays nuevos (después de mover)
        $response = $this->json('GET', "/api/move/right", $data);
        $response->assertStatus(200);
        $response->assertJsonStructure(['data'], $response->json());
        $response->assertJsonStructure(['blocks_coords', 'local_matrix'], $response->json()['data']);
        $actual = $response->json()['data']['blocks_coords'];

        $this->assertEquals($expected, $actual);
    }
    public function testMoveLeft() {
        // Array viejos (antes de mover)
        $data = [];
        $respuesta = $this->json('GET', "/api/get", $data);
        $respuesta->assertStatus(200);
        $respuesta->assertJsonStructure(['data'], $respuesta->json());
        $respuesta->assertJsonStructure(['blocks_coords', 'local_matrix'], $respuesta->json()['data']);
        $expected = array_map(function($item) {
            return [$item[0], $item[1] - 1];
        }, $respuesta->json()['data']['blocks_coords']);

        // Arrays nuevos (después de mover)
        $response = $this->json('GET', "/api/move/left", $data);
        $response->assertStatus(200);
        $response->assertJsonStructure(['data'], $response->json());
        $response->assertJsonStructure(['blocks_coords', 'local_matrix'], $response->json()['data']);
        $actual = $response->json()['data']['blocks_coords'];

        $this->assertEquals($expected, $actual);

}

    public function testRotateLeft() {
        // Array viejos (antes de mover)
        $data = [];
        $respuesta = $this->json('GET', "/api/get", $data);
        $respuesta->assertStatus(200);
        $respuesta->assertJsonStructure(['data'], $respuesta->json());
        $respuesta->assertJsonStructure(['blocks_coords', 'local_matrix'], $respuesta->json()['data']);

        $reverse = array_map(function ($row) {
            return array_reverse($row);
        }, $respuesta->json()['data']['local_matrix']);
        $expected_1 = $transpose = array_map(null, ...$reverse);

        // Arrays nuevos (después de mover)
        $response = $this->json('GET', "/api/rotate/left", $data);
        $response->assertStatus(200);
        $response->assertJsonStructure(['data'], $response->json());
        $response->assertJsonStructure(['blocks_coords', 'local_matrix'], $response->json()['data']);
        $actual_1 = $response->json()['data']['local_matrix'];

        $this->assertEquals($expected_1, $actual_1);
    }

    public function testRotateRight() {
        // Array viejos (antes de mover)
        $data = [];
        $respuesta = $this->json('GET', "/api/get", $data);
        $respuesta->assertStatus(200);
        $respuesta->assertJsonStructure(['data'], $respuesta->json());
        $respuesta->assertJsonStructure(['blocks_coords', 'local_matrix'], $respuesta->json()['data']);

        $transpose = array_map(null, ...$respuesta->json()['data']['local_matrix']);
        $expected_1 = $reverse = array_map(function ($row) {
            return array_reverse($row);
        }, $transpose);

        // Arrays nuevos (después de mover)
        $response = $this->json('GET', "/api/rotate/right", $data);
        $response->assertStatus(200);
        $response->assertJsonStructure(['data'], $response->json());
        $response->assertJsonStructure(['blocks_coords', 'local_matrix'], $response->json()['data']);
        $actual_1 = $response->json()['data']['local_matrix'];

        $this->assertEquals($expected_1, $actual_1);
    }

    protected function getBlocksCoords($init_row, $init_column, $local_matrix) {
        $blocks_coords = [];

        foreach ($local_matrix as $row_idx => $row) {
            foreach ($row as $column_idx => $column) {
                if ($column === 1) {
                    $blocks_coords[] = [$init_row - ($row_idx + 1), $init_column + $column_idx];
                }
            }
        }
        return $blocks_coords;
    }
}