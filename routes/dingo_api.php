<?php

use Dingo\Api\Routing\Router;

/*
|--------------------------------------------------------------------------
| Dingo API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$api = app('Dingo\Api\Routing\Router');

$api->version(config('api.version'), ['middleware' => ['web']], function(Router $api) {
    $api->group(['namespace' => 'App\Http\Controllers\API'], function(Router $api) {
        $api->group(['prefix' => "move"], function(Router $api) {
            $api->post('{direction?}', 'MoveController@move');
            $api->post('right', 'MoveController@moveRight');
            $api->post('left', 'MoveController@moveLeft');
            $api->post('down', 'MoveController@moveDown');
            $api->post('drop', 'MoveController@drop');
        });
        $api->group(['prefix' => "rotate"], function(Router $api) {
            $api->post('{direction?}', 'RotateController@rotate');
            $api->post('left', 'RotateController@rotateLeft');
            $api->post('right', 'RotateController@rotateRight');
        });
        $api->group(['prefix' => "status"], function(Router $api) {
            $api->get('show', 'StatusController@show');
        });
    });
});
