<?php

namespace App\Transformers;


use App\Models\TetrominoI;
use League\Fractal\TransformerAbstract;

class TetrominoITransformer extends TransformerAbstract {
    public function transform(TetrominoI $tetromino_i) {
        return [
            'blocks_coords' => $tetromino_i->blocks_coords,
            'local_matrix' => $tetromino_i->local_matrix
        ];
    }
}