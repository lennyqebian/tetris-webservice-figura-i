<?php

namespace App\Http\Controllers\API;

use App\Models\TetrominoI;
use App\Transformers\TetrominoITransformer;
use Dingo\Api\Routing\Helpers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RotateController extends Controller
{
    use Helpers;

    /**
     * @var TetrominoI
     */
    private $tetromino;

    /**
     * MoveController constructor.
     * @param TetrominoI $tetromino
     */
    public function __construct(TetrominoI $tetromino) {
        $this->tetromino = $tetromino;
    }

    public function rotateLeft(Request $request) {
        $this->tetromino = $request->session()->get('tetromino', $this->tetromino);
        $this->tetromino->rotate("left");
        $request->session()->put(['tetromino' => $this->tetromino]);
        return $this->response->item($this->tetromino, new TetrominoITransformer());
    }

    public function rotateRight(Request $request) {
        $this->tetromino = $request->session()->get('tetromino', $this->tetromino);
        $this->tetromino->rotate("right");
        $request->session()->put(['tetromino' => $this->tetromino]);
        return $this->response->item($this->tetromino, new TetrominoITransformer());
    }

    public function rotate(Request $request, $direction = "right") {
        $this->tetromino = $request->session()->get('tetromino', $this->tetromino);
        $this->tetromino->rotate($direction);
        $request->session()->put(['tetromino' => $this->tetromino]);
        return $this->response->item($this->tetromino, new TetrominoITransformer());
    }
}
