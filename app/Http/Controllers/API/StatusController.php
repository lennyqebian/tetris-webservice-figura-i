<?php

namespace App\Http\Controllers\API;

use App\Models\TetrominoI;
use App\Http\Controllers\Controller;
use App\Transformers\TetrominoITransformer;
use Dingo\Api\Http\Request;
use Dingo\Api\Routing\Helpers;

class StatusController extends Controller
{
    use Helpers;

    /**
     * @var TetrominoI
     */
    private $tetromino;

    /**
     * StatusController constructor.
     * @param TetrominoI $tetromino
     */
    public function __construct(TetrominoI $tetromino) {
        $this->tetromino = $tetromino;
    }

    public function show(Request $request) {
        $this->tetromino = $request->session()->get('tetromino', $this->tetromino);
        return $this->response->item($this->tetromino, new TetrominoITransformer());
    }
}
