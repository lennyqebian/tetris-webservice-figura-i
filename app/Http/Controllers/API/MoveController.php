<?php

namespace App\Http\Controllers\API;

use App\Models\TetrominoI;
use App\Http\Controllers\Controller;
use App\Transformers\TetrominoITransformer;
use Dingo\Api\Http\Request;
use Dingo\Api\Routing\Helpers;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class MoveController extends Controller
{
    use Helpers;

    /**
     * @var TetrominoI
     */
    private $tetromino;

    /**
     * MoveController constructor.
     * @param TetrominoI $tetromino
     */
    public function __construct(TetrominoI $tetromino) {
        $this->tetromino = $tetromino;
    }

    public function moveDown(Request $request) {
        $this->tetromino = $request->session()->get('tetromino', $this->tetromino);
        $this->tetromino->moveDown();
        $request->session()->put(['tetromino' => $this->tetromino]);
        return $this->response->item($this->tetromino, new TetrominoITransformer());
    }

    public function moveLeft(Request $request) {
        $this->tetromino = $request->session()->get('tetromino', $this->tetromino);
        $this->tetromino->moveLeft();
        $request->session()->put(['tetromino' => $this->tetromino]);
        return $this->response->item($this->tetromino, new TetrominoITransformer());
    }

    public function moveRight(Request $request) {
        $this->tetromino = $request->session()->get('tetromino', $this->tetromino);
        $this->tetromino->moveRight();
        $request->session()->put(['tetromino' => $this->tetromino]);
        return $this->response->item($this->tetromino, new TetrominoITransformer());
    }

    public function move(Request $request, $direction = "down") {
        $this->tetromino = $request->session()->get('tetromino', $this->tetromino);

        switch ($direction) {
            case "down":
                $this->tetromino->moveDown();
                break;
            case "left":
                $this->tetromino->moveLeft();
                break;
            case "right":
                $this->tetromino->moveRight();
                break;
            default:
                throw new BadRequestHttpException("¡La dirección ingresada no es válida!");
                break;
        }

        $request->session()->put(['tetromino' => $this->tetromino]);
        return $this->response->item($this->tetromino, new TetrominoITransformer());
    }
}
