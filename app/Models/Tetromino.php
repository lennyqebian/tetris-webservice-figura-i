<?php

namespace App\Models;


use BadMethodCallException;

abstract class Tetromino {
    /**
     * @var array
     */
    protected $local_matrix = [];

    /**
     * @var array
     */
    protected $rotation_axis_coords = [];

    /**
     * @var int
     */
    protected $init_row;

    /**
     * @var int
     */
    protected $init_column;

    /**
     * @var array
     */
    protected $board_coords = [];

    /**
     * @var array
     */
    protected $blocks_coords = [];

    /**
     * Tetromino constructor.
     */
    public function __construct() {
        $this->setBlocksCoords();
    }

    /**
     * @param $name
     * @return mixed
     */
    public function __get($name) {
        switch ($name) {
            case "blocks_coords":
                return $this->blocks_coords;
                break;
            case "local_matrix":
                return $this->local_matrix;
                break;
        }
    }

    public function setBlocksCoords() {
        $blocks_coords = [];

        foreach ($this->local_matrix as $row_idx => $row) {
            foreach ($row as $column_idx => $column) {
                if ($column === 1) {
                    $blocks_coords[] = [$this->init_row - ($row_idx + 1), $this->init_column + $column_idx];
                }
            }
        }
        $this->blocks_coords = $blocks_coords;
    }

    public function rotate($direction = "right") {
        if (!in_array($direction, ["left", "right"])) {
            throw new BadMethodCallException("Direction can only be to the left or right!");
        }

        $transpose = $this->local_matrix;

        if ($direction === "right") {
            $transpose = array_map(null, ...$this->local_matrix);
        }

        $result = array_map(function ($row) {
            return array_reverse($row);
        }, $transpose);

        if ($direction === "left") {
            $result = array_map(null, ...$result);
        }
        $this->local_matrix = $result;
        $this->setBlocksCoords();
    }

    public function moveDown() {
        if ($this->init_row > 0) {
            $this->init_row--;
            $this->setBlocksCoords();
        }
    }

    public function moveLeft() {
        if ($this->init_column > 0) {
            $this->init_column--;
            $this->setBlocksCoords();
        }
    }

    public function moveRight() {
        if ($this->init_column < 9 - 3) {
            $this->init_column++;
            $this->setBlocksCoords();
        }
    }
}