<?php

namespace App\Models;


class TetrominoI extends Tetromino {
    protected $local_matrix = [
        [0, 0, 0, 0],
        [1, 1, 1, 1],
        [0, 0, 0, 0],
        [0, 0, 0, 0],
    ];

    protected $rotation_axis_coords = [1, 2];

    protected $init_row = 21;

    protected $init_column = 3;
}
